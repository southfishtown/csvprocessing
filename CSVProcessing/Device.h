//
//  NameList.h
//  CSVReader
//
//  Created by Pu Zhao on 2019/10/29.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Device : NSObject{
    NSString * deviceName;
    NSString * deviceId;
}

@property NSString * deviceId;
@property NSString * deviceName;

-(id)initWithId:(NSString*)DeviceId andName:(NSString*)DeviceName;

@end

NS_ASSUME_NONNULL_END

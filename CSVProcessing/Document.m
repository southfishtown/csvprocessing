//
//  Document.m
//  CSVProcessing
//
//  Created by Pu Zhao on 2019/10/30.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import "Document.h"
#import "CHCSVParser.h"
#import "Device.h"

@interface Document ()<CHCSVParserDelegate>{
    NSMutableArray *_lines;
    NSMutableArray *_currentLine;
}

@property (weak) IBOutlet NSTableView *tableView;
@property (strong) IBOutlet NSArrayController *controller;

- (IBAction)loadFile:(id)sender;
- (IBAction)saveFile:(id)sender;

@property(strong, nonatomic) NSMutableArray* devices;
@property(strong, nonatomic) NSString* csvFileName;
@property(strong, nonatomic) NSString* tempDirectory;

@property bool isFileLoaded;

@end

@implementation Document
- (void)parserDidBeginDocument:(CHCSVParser *)parser {
    _lines = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber {
    _currentLine = [[NSMutableArray alloc] init];
}
- (void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex {
    NSLog(@"%@", field);
    [_currentLine addObject:field];
}
- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)recordNumber {
    [_lines addObject:_currentLine];
    _currentLine = nil;
}
- (void)parserDidEndDocument:(CHCSVParser *)parser {
    //    NSLog(@"parser ended: %@", csvFile);
}
- (void)parser:(CHCSVParser *)parser didFailWithError:(NSError *)error {
    NSLog(@"ERROR: %@", error);
    _lines = nil;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
        //Set file loading flag
        self.isFileLoaded = false;
        
        //Initialize temporary Devices.csv file
        self.csvFileName = @"Devices.csv";
        
        self.tempDirectory = NSTemporaryDirectory();
        //Load path string from label
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:self.csvFileName ofType:@""];
        NSString *targetPath = [self.tempDirectory stringByAppendingPathComponent:self.csvFileName];
        NSError *error = nil;
        
        //Copy Device.csv file from app directory to the temp directory
        NSFileManager* fileManager = [NSFileManager defaultManager];
        
        if([fileManager fileExistsAtPath:targetPath] && ![fileManager removeItemAtPath:targetPath error:&error]) {
            NSLog(@"Error Deleteing Existing File: %@", [error description]);
        }
        
        if (![fileManager copyItemAtPath:sourcePath toPath:targetPath error:&error])
        {
            NSLog(@"Error Copying: %@", [error description]);
        }
        
    }
    return self;
}

//Override the display name of the title bar
- (NSString *)displayName {
    if (![self fileURL])
        return @"CSVProcessing";

    return [super displayName];
}

+ (BOOL)autosavesInPlace {
    return YES;
}


- (NSString *)windowNibName {
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"Document";
}


- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError {
    // Insert code here to write your document to data of the specified type. If outError != NULL, ensure that you create and set an appropriate error if you return nil.
    // Alternatively, you could remove this method and override -fileWrapperOfType:error:, -writeToURL:ofType:error:, or -writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.
    [NSException raise:@"UnimplementedMethod" format:@"%@ is unimplemented", NSStringFromSelector(_cmd)];
    return nil;
}


- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError {
    // Insert code here to read your document from the given data of the specified type. If outError != NULL, ensure that you create and set an appropriate error if you return NO.
    // Alternatively, you could remove this method and override -readFromFileWrapper:ofType:error: or -readFromURL:ofType:error: instead.
    // If you do, you should also override -isEntireFileLoaded to return NO if the contents are lazily loaded.
    [NSException raise:@"UnimplementedMethod" format:@"%@ is unimplemented", NSStringFromSelector(_cmd)];
    return YES;
}


- (IBAction)loadFile:(id)sender {
    //Load import path
    NSString *importFilePath = [self.tempDirectory stringByAppendingPathComponent:self.csvFileName];
    
    //Convert path to NSURL
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:importFilePath];
      
    //Load csv file to NSArray using CHCSVParser
    NSArray *rows = [NSArray arrayWithContentsOfCSVURL:fileURL];

      //Check if the conversion is successful
    if(rows==nil){
          NSLog(@"error parsing csv file");
          return;
    }
      
      //_devices initialization
    _devices = [[NSMutableArray alloc] init];
      
    //Add rows to _devices
    NSMutableArray* tempDevices =[[NSMutableArray alloc] init];
    
    for(NSArray* ele in rows){
          
        Device * newDevice = [[Device alloc] initWithId:ele[0] andName:ele[1]];
          
        [tempDevices addObject:newDevice];
    }

    //Update property
    self.devices = tempDevices;
    
    //Set file load flag
    self.isFileLoaded = true;
}

- (IBAction)saveFile:(id)sender {
    //Load save path
    NSString *exportFilePath = [self.tempDirectory stringByAppendingPathComponent:self.csvFileName];
    
    NSOutputStream * exportStream = [NSOutputStream outputStreamToFileAtPath:exportFilePath append:false];
    
    //Write to csv file using CHCSVWriter
    CHCSVWriter *csvWriter = [[CHCSVWriter alloc] initWithOutputStream:exportStream encoding:NSUTF8StringEncoding delimiter:','];
    
    for( Device* d in self.devices){
        [csvWriter writeField:d.deviceId];
        [csvWriter writeField:d.deviceName];
        [csvWriter finishLine];
    }
    
}
@end

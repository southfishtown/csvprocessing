//
//  AppDelegate.h
//  CSVProcessing
//
//  Created by Pu Zhao on 2019/10/30.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end


//
//  NameList.m
//  CSVReader
//
//  Created by Pu Zhao on 2019/10/29.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import "Device.h"

@implementation Device

@synthesize deviceId;
@synthesize deviceName;

-(id) init {
    self = [super init];
    if(self){
        deviceId = @"0";
        deviceName = @"Unamed";
    }
    
    return self;
}


-(id)initWithId:(nonnull NSString*)dId andName:(nonnull NSString *)dName{
    if(self = [super init]){
        deviceId = dId;
        deviceName = dName;
    }
    return self;
}

-(bool) canRemove{
    return true;
}

@end

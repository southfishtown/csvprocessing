![](https://gitlab.com/southfishtown/csvprocessing/raw/master/Capture.PNG)

# Intro
This app reads, processes and writes a file named "Device.csv".

# Tags
objectivec, xib, without Storyboard, csv processing, Cocoa Binding

# Referenced package
https://github.com/davedelong/CHCSVParser
